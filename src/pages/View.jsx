import { useParams } from "react-router-dom";

const ViewPage = ({ userData }) => {
  const { userId } = useParams();
  const user = userData.find((user) => user.id === userId);

  if (!user) {
    return <div>User not found</div>;
  }

  return (
    <div>
      <h1>User Details</h1>
      <p>Name: {user.name}</p>
      <p>Address: {user.address}</p>
      <p>Hobby: {user.hobby}</p>
    </div>
  );
};

export default ViewPage;
