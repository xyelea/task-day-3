import React, { useState, useEffect } from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import {
  Avatar,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  Collapse,
  Divider,
  Grid,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
} from "@mui/material";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import FavoriteIcon from "@mui/icons-material/Favorite";
import ShareIcon from "@mui/icons-material/Share";
import { red } from "@mui/material/colors";
import axios from "axios";
import PropTypes from "prop-types";
import useAsync from "../Hooks/UseAsync";

export default function Day6() {
  const [showSidebar, setShowSidebar] = useState(true);
  const [postData, setPostData] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost:3004/postgenerated")
      .then((response) => {
        const data = response.data;
        setPostData(data);
      })
      .catch((error) => {
        console.error(error);
      });

    const handleResize = () => {
      setShowSidebar(window.innerWidth > 900);
    };

    window.addEventListener("resize", handleResize);
    handleResize();

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <div>
      <CssBaseline />
      <Container maxWidth="1200px">
        <Grid container spacing={2}>
          {/* card */}
          <Grid item xs={12} md={showSidebar ? 9 : 12}>
            <Box sx={{ height: "100vh", display: "flex" }} mt={2}>
              <Grid container spacing={2} m={2}>
                {postData.map((post, index) => (
                  <Grid item xs={12} md={showSidebar ? 6 : 12} key={index}>
                    <Card
                      sx={{
                        display: "flex",
                        flexDirection: "column",
                        height: "100%",
                      }}>
                      <CardHeader
                        avatar={
                          <Avatar sx={{ bgcolor: red[500] }}>
                            {post.author[0]}
                          </Avatar>
                        }
                        action={
                          <IconButton aria-label="settings">
                            <MoreVertIcon />
                          </IconButton>
                        }
                        title={post.title}
                        subheader={post.datePost}
                      />
                      <CardMedia
                        component="img"
                        height="194"
                        image={post.img}
                        alt={post.title}
                      />
                      <CardContent>
                        <Typography variant="body2" color="text.secondary">
                          {post.description}
                        </Typography>
                      </CardContent>
                      <CardActions disableSpacing>
                        <IconButton aria-label="add to favorites">
                          <FavoriteIcon />
                        </IconButton>
                        <IconButton aria-label="share">
                          <ShareIcon />
                        </IconButton>
                      </CardActions>
                      <Collapse in={false} timeout="auto" unmountOnExit>
                        <CardContent>
                          <Typography paragraph>Additional Content:</Typography>
                          <Typography paragraph>
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Aenean et tortor nunc. Ut consequat purus ut
                            posuere rutrum. In convallis venenatis erat non
                            consequat. Nullam pharetra, neque vitae scelerisque
                            malesuada, lectus est feugiat massa, sed vulputate
                            lectus lacus in lacus. Suspendisse et tincidunt
                            risus. Proin consectetur id nulla id consectetur.
                            Nunc nec tempus nunc.
                          </Typography>
                        </CardContent>
                      </Collapse>
                    </Card>
                  </Grid>
                ))}
              </Grid>
            </Box>
          </Grid>
          {/* item */}
          {showSidebar && (
            <Grid item md={3}>
              <Box sx={{ height: "100vh" }} mt={2}>
                <List>
                  {/* Item 1 */}
                  <ListItem>
                    <ListItemIcon>
                      {/* Icon */}
                      <ShareIcon />
                    </ListItemIcon>
                    <ListItemText primary="Item 1" secondary="June 1, 2023" />
                  </ListItem>
                  <Divider />
                  {/* Item 2 */}
                  <ListItem>
                    <ListItemIcon>
                      {/* Icon */}
                      <MoreVertIcon />
                    </ListItemIcon>
                    <ListItemText primary="Item 2" secondary="June 2, 2023" />
                  </ListItem>
                  <Divider />
                  {/* Item 3 */}
                  <ListItem>
                    <ListItemIcon>
                      {/* Icon */}
                      <FavoriteIcon />
                    </ListItemIcon>
                    <ListItemText primary="Item 3" secondary="June 3, 2023" />
                  </ListItem>
                </List>
              </Box>
            </Grid>
          )}
        </Grid>
      </Container>
    </div>
  );
}

Card.propTypes = {
  post: PropTypes.shape({
    author: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    datePost: PropTypes.string.isRequired,
    img: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
  }).isRequired,
};
