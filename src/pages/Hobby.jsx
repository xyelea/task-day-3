import React from "react";

const Hobby = ({ userData }) => {
  return (
    <div>
      <h1>Hobby</h1>
      <ul>
        {userData.map((user) => (
          <li key={user.id}>
            <strong>{user.name}</strong> - {user.hobby}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Hobby;
