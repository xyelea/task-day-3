import React, { useState } from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import TableContainer from "@mui/material/TableContainer";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableBody from "@mui/material/TableBody";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import { Button, Modal, TextField } from "@mui/material";
import { useNavigate } from "react-router-dom";

export default function SimpleContainer({ userData, onEditUser }) {
  const [open, setOpen] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");
  const [editedUser, setEditedUser] = useState({
    id: "",
    name: "",
    address: "",
    hobby: "",
  });
  const navigate = useNavigate();

  const handleView = (userId) => {
    navigate(`/view/${userId}`);
  };

  const handleOpen = (user) => {
    setEditedUser(user);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleInputChange = (event) => {
    setEditedUser((prevUser) => ({
      ...prevUser,
      [event.target.name]: event.target.value,
    }));
  };

  const handleSave = () => {
    onEditUser(editedUser.id, editedUser);
    handleClose();
  };

  const filteredData = userData.filter(
    (user) =>
      user.name.toLowerCase().includes(searchTerm.toLowerCase()) ||
      user.address.toLowerCase().includes(searchTerm.toLowerCase()) ||
      user.hobby.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return (
    <React.Fragment>
      <CssBaseline />
      <Container>
        <Box
          sx={{
            display: "flex",
            height: "100vh",
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "column",
          }}>
          <AccountCircleIcon sx={{ fontSize: "50vh", color: "#F7B531" }} />
          <Typography variant="h1" color="#FF5757">
            User
          </Typography>
          <TextField
            label="Search"
            value={searchTerm}
            onChange={(event) => setSearchTerm(event.target.value)}
            mb={2}
          />

          <TableContainer>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell>Address</TableCell>
                  <TableCell>Hobby</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {filteredData.map((user, index) => (
                  <TableRow key={index}>
                    <TableCell>{user.name}</TableCell>
                    <TableCell>{user.address}</TableCell>
                    <TableCell>{user.hobby}</TableCell>
                    <TableCell>
                      <Box sx={{ display: "flex", gap: 1 }}>
                        <Button
                          color="primary"
                          variant="outlined"
                          onClick={() => handleView(user.id)}>
                          View
                        </Button>

                        <Button
                          color="primary"
                          variant="outlined"
                          onClick={() => handleOpen(user)}>
                          Edit
                        </Button>
                      </Box>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
      </Container>
      <Modal open={open} onClose={handleClose}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            width: 400,
            bgcolor: "background.paper",
            border: "2px solid #000",
            boxShadow: 24,
            p: 4,
          }}>
          <Typography variant="h6" component="h2" mb={2}>
            Edit User
          </Typography>
          <TextField
            fullWidth
            label="Name"
            name="name"
            value={editedUser.name}
            onChange={handleInputChange}
            mb={2}
          />
          <TextField
            fullWidth
            label="Address"
            name="address"
            value={editedUser.address}
            onChange={handleInputChange}
            mb={2}
          />
          <TextField
            fullWidth
            label="Hobby"
            name="hobby"
            value={editedUser.hobby}
            onChange={handleInputChange}
            mb={2}
          />
          <Button variant="contained" color="primary" onClick={handleSave}>
            Save
          </Button>
        </Box>
      </Modal>
    </React.Fragment>
  );
}
