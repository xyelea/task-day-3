import React, { useState } from "react";
import {
  AppBar,
  Box,
  Toolbar,
  Typography,
  Button,
  Modal,
  TextField,
} from "@mui/material";
import { Link } from "react-router-dom";

export const Navbar = ({ onUserData, onEditUser }) => {
  const [open, setOpen] = useState(false);
  const [userData, setUserData] = useState({
    name: "",
    address: "",
    hobby: "",
  });
  const [editMode, setEditMode] = useState(false);
  const [editUserId, setEditUserId] = useState("");

  const createTheme = (primaryColor) => {
    return {
      bgcolor: primaryColor,
    };
  };

  const handleOpen = () => {
    setUserData({
      name: "",
      address: "",
      hobby: "",
    });
    setOpen(true);
    setEditMode(false);
    setEditUserId("");
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleInputChange = (event) => {
    setUserData((prevUserData) => ({
      ...prevUserData,
      [event.target.name]: event.target.value,
    }));
  };

  const handleSubmit = () => {
    if (editMode) {
      onEditUser(editUserId, userData);
    } else {
      onUserData({ id: Math.random().toString(), ...userData });
    }
    handleClose();
  };

  const handleEdit = (user) => {
    setUserData(user);
    setEditMode(true);
    setEditUserId(user.id);
    setOpen(true);
  };

  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  const textFieldStyle = {
    mb: 2,
  };

  const buttonStyle = {
    mt: 2,
  };

  const toolbarStyle = {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  };

  const buttonContainerStyle = {
    display: "flex",
    gap: "8px",
  };

  const flexContainerStyle = {
    display: "flex",
    gap: "8px",
  };

  // Mengambil nilai warna dari file .env
  const primaryColor = process.env.REACT_APP_COLOR_PRIMARY || "red";

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" sx={createTheme(primaryColor)}>
        <Toolbar sx={toolbarStyle}>
          <Box sx={flexContainerStyle}>
            <Link to="/" style={{ textDecoration: "none" }}>
              <Typography variant="h6" color={"white"}>
                App Kito
              </Typography>
            </Link>
            <Link to="/hobby" style={{ textDecoration: "none" }}>
              <Button color="inherit" sx={{ color: "white" }}>
                Hobby
              </Button>
            </Link>
            <Link to="/about" style={{ textDecoration: "none" }}>
              <Button color="inherit" sx={{ color: "white" }}>
                About
              </Button>
            </Link>
            <Link to="/day-6" style={{ textDecoration: "none" }}>
              <Button color="inherit" sx={{ color: "white" }}>
                Day-6
              </Button>
            </Link>
          </Box>
          <Box sx={buttonContainerStyle}>
            <Button color="inherit" onClick={handleOpen} variant="outlined">
              Add user
            </Button>
          </Box>
        </Toolbar>
      </AppBar>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description">
        <Box sx={style}>
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
            sx={textFieldStyle}>
            {editMode ? "Edit User" : "Add User"}
          </Typography>
          <TextField
            fullWidth
            label="Name"
            id="fullWidth"
            sx={textFieldStyle}
            name="name"
            value={userData.name}
            onChange={handleInputChange}
          />
          <TextField
            fullWidth
            label="Address"
            id="fullWidth"
            sx={textFieldStyle}
            name="address"
            value={userData.address}
            onChange={handleInputChange}
          />
          <TextField
            fullWidth
            label="Hobby"
            id="fullWidth"
            sx={textFieldStyle}
            name="hobby"
            value={userData.hobby}
            onChange={handleInputChange}
          />
          <Button
            color="primary"
            variant="outlined"
            sx={buttonStyle}
            onClick={handleSubmit}>
            {editMode ? "Save" : "Add"}
          </Button>
        </Box>
      </Modal>
    </Box>
  );
};
