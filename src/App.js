import React, { useState } from "react";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import { Navbar } from "./components/Navbar";
import SimpleContainer from "./components/SimpleContainer";
import Hobby from "./pages/Hobby";
import AboutPage from "./pages/About";
import ViewPage from "./pages/View";
import Day6 from "./pages/Day6";

function App() {
  const [userData, setUserData] = useState([
    // Data pengguna
    {
      id: "1",
      name: "Alice Mcbride",
      address: "123 Main St",
      hobby: "Reading",
    },
    {
      id: "2",
      name: "Slade Hull",
      address: "456 Elm St",
      hobby: "Gardening",
    },
    {
      id: "3",
      name: "Melvin Lloyd",
      address: "789 Oak St",
      hobby: "Cooking",
    },
    {
      id: "4",
      name: "Marah Clements",
      address: "321 Pine St",
      hobby: "Painting",
    },
    {
      id: "5",
      name: "Lavinia Lynn",
      address: "987 Maple St",
      hobby: "Photography",
    },
  ]);

  const handleUserData = (newUserData) => {
    // Fungsi untuk menambahkan data pengguna
    setUserData((prevUserData) => [...prevUserData, newUserData]);
  };

  const handleEditUser = (userId, updatedUserData) => {
    // Fungsi untuk mengedit data pengguna
    setUserData((prevUserData) =>
      prevUserData.map((user) =>
        user.id === userId ? { ...user, ...updatedUserData } : user
      )
    );
  };

  return (
    <Router>
      <div>
        <Navbar onUserData={handleUserData} onEditUser={handleEditUser} />
        <Routes>
          <Route
            path="/"
            element={
              <SimpleContainer
                userData={userData}
                onEditUser={handleEditUser}
              />
            }
          />
          <Route path="/hobby" element={<Hobby userData={userData} />} />

          <Route path="/about" element={<AboutPage />} />
          <Route path="/day-6" element={<Day6 />} />
          <Route
            path="/view/:userId"
            element={<ViewPage userData={userData} />}
          />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
